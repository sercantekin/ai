package com.AI;
import java.util.Arrays;
/*
    The way it is working as follows:
    1. create a random array which consist of row numbers of queens positions. (format: [2, 4, 1, 3])
    2. on each step of temperature decreasing, iterate as many as max iteration number
    3. on each iteration, make move to find an conflict-free position for queens
    4. to do that check each queen with others to see if there is any other queen on same row number
    5. also check consecutive queens to make sure that row numbers difference must be bigger than 1
    6. if you cannot find better position use probability equation to see whether you will use the position you  started the move
        or the current position for next loop (p = exp(dE/t) ==> p > rand[0-1]). return when you find better position
    7. when you find conflict free solution, return the solution for printing in 2 dimensional array
*/
public class Main {

    private static double lastTemp;
    private static int totalIteration;

    public static void main(String[] args) {
        // parameters
        int n = 100;
        double logE075 = 0.287682; // log.e(0.75) to calculate start temp based on n
        int max_iteration = 30;
        double startTemp = n * logE075;
        double coolFac = 0.1;

        // calling simulated annealing function
        int[] result = simulatedAnnealing(n, max_iteration, startTemp, coolFac);

        // printing result
        assert result != null;
        printResult(result, n);
    }

    // ---------------------------------------------------------------------------------------------
    // simulated annealing function
    private static int[] simulatedAnnealing(int n, int maxNumOfIterations, double temperature, double coolingFactor) {
        // as prof recommended, starts with random position
        int[] qArray = generateRandomState(n);

        // how far from solution
        int conflictNumber = howManyConflict(qArray, n);

        // terminate when it reaches max_iteration or problem is solved. same time reduce temperature
        while (temperature > 0.001){
            for (int i = 0; i < maxNumOfIterations; i++) {
                makeMove(qArray, conflictNumber, temperature, n);
                conflictNumber = howManyConflict(qArray, n);
                System.out.println("Iteration: " + totalIteration + "\t\tNumber of conflict: " + conflictNumber);
                totalIteration++;
                if(conflictNumber == 0){
                    lastTemp = temperature;
                    return qArray; // return solution if conflict-free
                }
            }
            temperature -= temperature * coolingFactor;
        }
        return null; // return null if no solution at the end
    }

    // making move
    private static void makeMove(int[] qArray, int costToBeat, double temp, int n) {
        while (true) {
            // creating random col and row number
            int nCol = (int) (Math.random() * n); // math.random returns double between 0-1
            int nRow = (int) (Math.random() * n); // type casting make the number integer within array index limit
            // swapping
            int tmpRow = qArray[nCol];
            qArray[nCol] = nRow;
            // after swapping, check number of conflicts again,
            int cost = howManyConflict(qArray, n);
            // if conflict's number is smaller than current situation, accept this move and return
            if (cost < costToBeat){
                break;
            }
            // if conflict's number is not smaller than current situation, calculate delta conflict between swapped position and current position
            int dE = costToBeat - cost;
            // then calculate accepted probability
            double acceptProb = Math.min(1, Math.exp(dE / temp));
            // compare probability with random double between 0-1
            if (Math.random() < acceptProb){
                break;
            }
            // if this is not true either, go back to position before swapping and continue for another random position
            qArray[nCol] = tmpRow;
        }
    }

    // generates random initial state
    private static int[] generateRandomState(int n) {
        int[] qArray = new int[n];
        for (int i = 0; i < n; i++){
            qArray[i] = (int) (Math.random() * n);
        }
        return qArray;
    }

    // Calculating numbers of conflicts
    private static int howManyConflict(int[] qArray, int n) {
        int conflict = 0;
        // if two queens are in same row or they are diagonal
        for (int i = 0; i < n; i++){
            for (int j = i + 1; j < n; j++){
                if (qArray[i] == qArray[j] || Math.abs(qArray[i] - qArray[j]) == j - i){
                    conflict += 1;
                }
            }
        }
        return conflict;
    }

    // print function for result
    private static void printResult(int[] result, int n){
        // creating array to print result, and initializing it
        String[][] printResult = new String[n][n];
        for (int row = 0; row < n; row ++){
            for (int col = 0; col < n; col++){
                printResult[row][col] = "-";
            }
        }

        // writing Queens on the result positions
        for (int row = 0; row < n; row ++) {
            printResult[row][result[row]] = "Q";
        }

        // printing result
        for(int i = 0; i < n; i++){
            System.out.println(Arrays.deepToString(printResult[i]));
        }
        System.out.println("\nTotal iteration: " + totalIteration + " and the temperature that solution has been found in: " + lastTemp);
    }
}
