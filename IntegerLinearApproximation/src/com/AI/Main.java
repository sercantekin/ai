package com.AI;

import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        int f;
        int optRes = 0;
	    int P01 = 17, P02 = 15;
	    int upperBound = 10;
	    int x1, x2;
	    int fitness1, fitness2;
	    int maxIteration = 1000;

	    for (int i = 0; i < maxIteration; i++){
            x1 = ThreadLocalRandom.current().nextInt(0, upperBound);
            x2 = ThreadLocalRandom.current().nextInt(0, upperBound);
            int constrain1 = 2*x1 + 4*x2;
            int constrain2 = 10*x1 + 3*x2;
            if (constrain1 <= P01 && constrain2 <= P02){
                f = x1 + 4*x2;
                if (f > optRes){
                    optRes = f;
                }
            }
            fitness1 = constrain1 - P01;
            fitness2 = constrain2 - P02;
            System.out.println("Iteration number = " + i + "\t\tX1 = " + x1 + "\t\tX2 = " + x2 + "\t\tFitness1 = "
                    + fitness1 + "\t\tFitness2 = " + fitness2 + "\t\t Max value = " + optRes);
            if (fitness1 == 0 && fitness2 == 0){
                break;
            }
        }
	    System.out.println("Optimal solution is " + optRes);
    }
}
